package org.example.framework.security.auth.principal;

import lombok.RequiredArgsConstructor;
import org.example.framework.server.auth.Principal;
import org.example.framework.server.auth.Roles;

@RequiredArgsConstructor
public class CertificatePrincipal implements Principal {
  private final String commonName;
  private static final Roles role = Roles.USER;

  @Override
  public String getName() {
    return commonName;
  }

  @Override
  public Roles getRole() {
    return role;
  }
}
