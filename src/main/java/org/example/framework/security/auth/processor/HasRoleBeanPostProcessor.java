package org.example.framework.security.auth.processor;

import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.example.framework.di.processor.BeanPostProcessor;
import org.example.framework.security.auth.annotation.HasRole;
import org.example.framework.security.auth.exception.UserNotAuthenticatedException;
import org.example.framework.server.auth.Roles;
import org.example.framework.server.auth.SecurityContext;

import java.lang.reflect.Method;


@Slf4j
public class HasRoleBeanPostProcessor implements BeanPostProcessor {
    @Override
    public boolean canProcessed(final Class<?> clazz) {
        final Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(HasRole.class)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object process(Object object, Class<?> clazz) {
        final Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(new MethodInterceptor() {
            @Override
            public Object intercept(final Object obj, final Method method, final Object[] arguments, final MethodProxy proxy) throws Throwable {
                if (!method.isAnnotationPresent(HasRole.class)) {
                    return method.invoke(object, arguments);
                }
                if (!SecurityContext.getPrincipal().getRole().equals(Roles.USER)) {
                    throw new UserNotAuthenticatedException();
                }
                return method.invoke(object, arguments);
            }
        });
        return clazz.cast(enhancer.create());
    }
}
