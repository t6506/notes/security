package org.example.framework.security.middleware;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.security.auth.exception.AuthenticationException;
import org.example.framework.security.auth.exception.BadAuthenticationException;
import org.example.framework.security.auth.principal.LoginPrincipal;
import org.example.framework.server.auth.SecurityContext;
import org.example.framework.server.http.HttpHeader;
import org.example.framework.server.http.Request;
import org.example.framework.server.middleware.Middleware;

import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Base64;


@Slf4j
@RequiredArgsConstructor

/*
  Request must contain a http header with key: "Authorization" and value "Basic encodedLoginPassword",
  where encodedLoginPassword is a colon-separated combination of username and password (username:password) encoded in Base64
*/

public class BasicAuthMiddleware implements Middleware {
  private static final String SCHEME = "Basic ";
  private static final String NOT_AUTHENTICATED = "not authenticated";
  private final Authenticator authenticator;

  @Override
  public void handle(final Socket socket, final Request request) {

    if (SecurityContext.getPrincipal() != null) {
      return;
    }

    try {
      String headerAuth = request.getHeaders().get(HttpHeader.AUTHORIZATION.value().toLowerCase());
      if (headerAuth == null) {
        log.error(NOT_AUTHENTICATED);
        return;
      }
      if (!headerAuth.startsWith(SCHEME)) {
        log.error(NOT_AUTHENTICATED);
        return;
      }
      byte[] decoded = Base64.getDecoder().decode(headerAuth.substring(SCHEME.length()));
      String[] parts = new String(decoded, StandardCharsets.UTF_8).split(":", 2);
      if (parts.length != 2) {
        log.error("invalid authorization header: {}", headerAuth);
        throw new BadAuthenticationException("invalid authorization header format");
      }
      final String login = parts[0];
      final String password = parts[1];

      LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(login, password);
      if (!authenticator.authenticate(authRequest)) {
        throw new AuthenticationException("can't authenticate");
      }

      SecurityContext.setPrincipal(new LoginPrincipal(login));
      log.debug("authenticated: {}", login);


    } catch (Exception e) {
      SecurityContext.clear();
      log.error(NOT_AUTHENTICATED);
    }
  }
}
