package org.example.framework.security.middleware;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.security.auth.exception.AuthenticationException;
import org.example.framework.security.auth.principal.LoginPrincipal;
import org.example.framework.server.auth.SecurityContext;
import org.example.framework.server.http.Request;
import org.example.framework.server.middleware.Middleware;

import java.net.Socket;


@Slf4j
@RequiredArgsConstructor
public class QueryAuthMiddleware implements Middleware {
  private final Authenticator authenticator;

  @Override
  public void handle(final Socket socket, final Request request) {
    if (SecurityContext.getPrincipal() != null) {
      return;
    }
    try {
      final String login = request.getQueryParams().get("login").get(0);
      final String password = request.getQueryParams().get("password").get(0);

      LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(login, password);
      if (!authenticator.authenticate(authRequest)) {
        throw new AuthenticationException("can't authenticate");
      }

      SecurityContext.setPrincipal(new LoginPrincipal(login));
      log.debug("authenticated: {}", login);
    } catch (Exception e) {
      SecurityContext.clear();
      log.error("not authenticated");
    }
  }
}
